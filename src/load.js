const fs = require("fs").promises;
const parse = require("./parse");

async function load(path) {
    const raw = await fs.readFile(path);
    return parse(raw);
}

module.exports = load;
