/**
 *
 * @param {object} obj
 */
function stringify(obj) {
    if (typeof obj == "function") return obj.toString();
    if (typeof obj == "string") return `\"${obj}\"`;
    if (typeof obj != "object") return obj;

    const params = [];

    var str = "{";

    Object.entries(obj).forEach(e => {
        var param = "";
        param += e[0] + ":";
        param += stringify(e[1]);

        params.push(param);
    });
    str += params.join(",");

    str += "}";

    return str;
}

module.exports = stringify;
