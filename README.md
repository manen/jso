# JSO

I could go on and on about what JSO is, but I won't.
Because it's just basically JSON but it supports functions.

## Usage

Using JSO is almost as simple as using JSON. All you have to do is get JSO using

```bash
npm install jso-json # the name jso was taken \_(o.o)_/
```

now, go into the code and import what you just installed.

```javascript
const jso = require("jso-json");
```

Now, you can use it! For example:

```javascript
const str = jso.stringify({
    a: "A string",
    b: () => {
        return "This is a stringified function!";
    },
    c: 4
});
// Outputs a string

console.log(str);

const obj = jso.parse(str); // Outputs an object

console.log(obj);

console.log(obj.b()); // Output: This is a stringified function!
```

## That's it!
