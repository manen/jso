const jso = require("../index");

const testObj = {
    obj: {
        thisIs: "A string!",
        butThis: 1,
        wasANumber: {
            reaction: "AMAZING!",
            react: () => {
                return "Wow! You just stringified a function! Crazy, huh?";
            }
        }
    }
};

console.time("total");
console.time("stringify");

const resultStr = jso.stringify(testObj);

console.log("Stringify result: ");
console.log(resultStr);

console.timeLog("stringify");

console.time("parse");

const resultObj = jso.parse(resultStr);

console.log(resultObj.obj.wasANumber.react());

console.log("Parse result: ");
console.log(resultObj);

console.timeLog("parse");

console.time("load");

jso.load("./test/test.jso").then(p => {
    console.log(p.a());

    console.timeLog("load");

    console.timeLog("total");
});
